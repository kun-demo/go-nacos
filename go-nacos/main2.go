package main

import (
	"fmt"
	"github.com/nacos-group/nacos-sdk-go/clients"
	"github.com/nacos-group/nacos-sdk-go/common/constant"
	"github.com/nacos-group/nacos-sdk-go/vo"
	"log"
)

func main() {
	//配置连接信息
	serverConfigs := []constant.ServerConfig{
		{
			IpAddr:      "192.168.0.204",
			ContextPath: "/nacos",
			Port:        8848,
			Scheme:      "http",
		},
	}

	// Create naming client for service discovery
	configClient, err := clients.CreateConfigClient(map[string]interface{}{
		"serverConfigs": serverConfigs,
	})
	if err != nil {
		log.Fatal("dddd:",err)
	}

	//读取文件
	content,err := configClient.GetConfig(vo.ConfigParam{
		DataId: "user-web.yaml",//此处对应之前的网页配置的名称
		Group: "dev",//此处对应之前的网页配置的分组
	})

	if err != nil {
		log.Fatal("xxxx:",err)
	}
	fmt.Println("content:",content)
}

