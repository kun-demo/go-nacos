package main

import (
	"fmt"
	"log"

	"github.com/nacos-group/nacos-sdk-go/clients"
	"github.com/nacos-group/nacos-sdk-go/common/constant"
	"github.com/nacos-group/nacos-sdk-go/vo"
)

func main() {
	nacosTest()
}

// 我通过example的源码 创建一个真正的注册中心
func nacosTest() {
	client, err := clients.CreateNamingClient(map[string]interface{}{
		"serverConfigs": []constant.ServerConfig{
			{
				IpAddr: "192.168.0.204",
				Port:   8848,
			},
		},
		"clientConfig": constant.ClientConfig{
			TimeoutMs:           5000,
			ListenInterval:      10000,
			NotLoadCacheAtStart: true,
			LogDir:              "log",
			NamespaceId:         "5aefcbc8-6e69-43ba-b112-fa871d72c1cb",
			//Group: "dev",//此处对应之前的网页配置的分组
			//Username:			 "nacos",
			//Password:			 "nacos",
		},
	})

	if err != nil {
		panic(err)
	}
	success, err := client.RegisterInstance(vo.RegisterInstanceParam{
		Ip:          "192.168.0.204",
		Port:        8848,
		ServiceName: "demo.go",
		//Group: "dev",//此处对应之前的网页配置的分组
		Weight:      10,
		Enable:      true,
		Healthy:     true,
		Ephemeral:   true,
	})
	if !success {
		log.Fatalln("注册nacos失败: ", err)
		return
	}
	service, _ := client.GetService(vo.GetServiceParam{
		ServiceName: "demo.go",
		//Group: "dev",//此处对应之前的网页配置的分组
	})
	fmt.Println("Service：")
	fmt.Println(service)
	return
}
