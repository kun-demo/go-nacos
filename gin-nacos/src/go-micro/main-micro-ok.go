package main

import (
	"context"
	"crypto/tls"
	"fmt"
	"github.com/asim/go-micro/v3/registry"
	"github.com/asim/go-micro/v3/web"
	"github.com/gin-gonic/gin"
	//nacos "github.com/micro/go-plugins/registry/nacos/v2"
	nacos "gitee.com/jawide/go-micro-nacos"
	"net/http"
)

func main() {
	rou := initGin()
	reg := initReg()
	ser := initService(rou, reg)
	//获取所有服务
	fmt.Println(reg.ListServices())

	err := ser.Run()
	if err != nil {
		panic(err)
	}
}

func initService(rou *gin.Engine, reg registry.Registry) web.Service {
	// service init
	ser := web.NewService(
		web.Name("go-micro-nacos"),
		web.Registry(reg),
		web.Handler(rou),
		//web.Address("0.0.0.0:8081"),
		web.Address(":8081"),


	)
	return ser
}

func initReg() registry.Registry {
	nacosNamespace := "test"
	group := "test"
	fmt.Println("namespace:",nacosNamespace,"group:",group)
	// reg init
	reg := nacos.NewRegistry(func(options *registry.Options) {
		options.Addrs = []string{"192.168.0.204:8848"}
		config:=tls.Config{ ServerName:"go-micro-test"}
		options.TLSConfig = &config

		// 支持 namespace
		options.Context = context.WithValue(context.Background(), &nacos.NacosNamespaceContextKey{}, nacosNamespace)
		//options.Context = context.WithValue(context.Background(), &nacos.NacosGroupContextKey{}, group)

	})

	//获取所有服务
	fmt.Println(registry.ListServices())
	//获取某一个服务
	services, err := registry.GetService("ljd-gateway")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(services)

	return reg
}

func initGin() *gin.Engine {
	// gin init
	r := gin.Default()
	r.GET("/hello", func(context *gin.Context) {
		context.String(200, "hello")
	})
	r.GET("/health", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"status":  "UP",
		})
		fmt.Println("health")
	})

	r.GET("/actuator/health", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"status":  "UP",
		})
		fmt.Println("/actuator/health")
	})
	return r
}
