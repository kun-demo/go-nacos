package main

import (
	"context"
	nacos "gitee.com/jawide/go-micro-nacos"
	"github.com/asim/go-micro/v3"
	"github.com/asim/go-micro/v3/registry"
)

func main() {
	nacosNamespace := "my_namespace"

	registry := nacos.NewRegistry(func(options *registry.Options) {
		options.Addrs = []string{"192.168.0.204:8848"}
		// 支持 namespace
		options.Context = context.WithValue(context.Background(), &nacos.NacosNamespaceContextKey{}, nacosNamespace)
	})
	service := micro.NewService(
		micro.Name("go.micro.src.demo"),
		micro.Registry(registry),
	)
	service.Run()

}

