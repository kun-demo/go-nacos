package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)


func main() {
	http.HandleFunc("/sidecar", sidecar)
	http.HandleFunc("/health", health)

	log.Fatal(http.ListenAndServe(":8089", nil))
}
func sidecar(w http.ResponseWriter, r *http.Request) {
	_, _ = fmt.Fprintf(w, "hello spring cloud alibaba sidecar")
}

func health(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	actuator := make(map[string]string)
	actuator["status"] = "UP"
	fmt.Println("ddd",actuator)
	_ = json.NewEncoder(w).Encode(actuator)
}
