package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

// https://www.jianshu.com/p/a31e4ee25305

func main() {
	router := initGin()


	err := router.Run(":8089")
	if err != nil {
		panic(err)
	}

	log.Fatal(http.ListenAndServe(":8089", nil))
}

func initGin() *gin.Engine {
	// gin init
	router := gin.Default()

	router.GET("/", func(c *gin.Context) {
		c.String(http.StatusOK, "index.html")
	})

	router.GET("/hello", func(context *gin.Context) {
		context.String(200, "hello")
	})

	router.GET("/health", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"status":  "UP",
		})
		fmt.Println("health")
	})

	router.GET("/actuator/health", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"status":  "UP",
		})
		fmt.Println("/actuator/health")
	})


	v1 := router.Group("/v1")

	v1.GET("/login", func(c *gin.Context) {
		c.String(http.StatusOK, "v1 login")
	})
	v2 := router.Group("/v2")

	v2.GET("/login", func(c *gin.Context) {
		c.String(http.StatusOK, "v2 login")
	})

	//	user
	user := router.Group("/user")


	user.GET("/", func(c *gin.Context) {

		 //dat:= map[string]interface{}
		allUsers := []UserModel{{ID: 123, Name: "张三", Age: 20}, {ID: 456, Name: "李四", Age: 25}}

		c.IndentedJSON(200, allUsers)


	})

	user.GET("/:name",func (c *gin.Context)  {
		name := c.Param("name")
		c.JSON(http.StatusOK, gin.H{
			"name":  name,
			"age":111,
			"sex":1,
			"id":11,
		})
	})

	user.PUT("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"status": gin.H{
				"status_code":http.StatusOK,
				"status": "ok",
			},
			"data": gin.H{
				"name":  "xxx",
				"age":111,
				"sex":1,
				"id":11,
			},
			"msg":  "xxx",
		})
	})

	user.POST("/", func(c *gin.Context) {
		id := c.Query("id")
		page := c.DefaultQuery("page", "0")
		name := c.PostForm("name")
		message := c.PostForm("message")
		fmt.Printf("id: %s; page: %s; name: %s; message: %s \n", id, page, name, message)

		c.JSON(http.StatusOK, gin.H{
			"status": gin.H{
				"status_code":http.StatusOK,
				"status": "ok",
			},
			"data": gin.H{
				"name":  "xxx",
				"age":111,
				"sex":1,
				"id":11,
			},
			"msg":  "xxx",
		})
	})

	user.DELETE("/:id",func (c *gin.Context)  {
		id := c.Param("id")
		c.JSON(http.StatusOK,  gin.H{
			"status": gin.H{
				"status_code":http.StatusOK,
				"status": "ok",
			},
			"data": gin.H{
				"name":  "xxx",
				"age":11,
				"sex":1,
				"id":id,
			},
			"msg":  "xxx",
		})
	})



	//router.Run(":8899")

	return router



}

type UserModel struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
	Age  int    `json:"age"`
	Sex  int    `json:"sex"`
}


