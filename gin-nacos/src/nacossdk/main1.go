package main

import (
	"fmt"
	"github.com/nacos-group/nacos-sdk-go/clients"
	"github.com/nacos-group/nacos-sdk-go/common/constant"
	"github.com/nacos-group/nacos-sdk-go/vo"
)

func main() {
	clientConfig := constant.ClientConfig{
		TimeoutMs:      10 * 1000, //http请求超时时间，单位毫秒
		ListenInterval: 30 * 1000, //监听间隔时间，单位毫秒（仅在ConfigClient中有效）
		BeatInterval:   5 * 1000, //心跳间隔时间，单位毫秒（仅在ServiceClient中有效）
		NamespaceId:       "", //nacos命名空间
		Endpoint:          "", //获取nacos节点ip的服务地址
		CacheDir:         "D:\\temp\\cache", //缓存目录
		LogDir:         "D:\\temp\\log", //日志目录
		UpdateThreadNum:   20, //更新服务的线程数
		NotLoadCacheAtStart: true, //在启动时不读取本地缓存数据，true--不读取，false--读取
		UpdateCacheWhenEmpty: true, //当服务列表为空时是否更新本地缓存，true--更新,false--不更新
	}

	// 至少一个
	serverConfigs := []constant.ServerConfig{
		{
			IpAddr:      "192.168.0.204",
			ContextPath: "/nacos",
			Port:        8848,
		},
	}


	configClient, err := clients.CreateConfigClient(map[string]interface{}{
		"serverConfigs": serverConfigs,
		"clientConfig":  clientConfig,
	})
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("configClient",configClient)

	success, err := configClient.PublishConfig(vo.ConfigParam{
		DataId:  "testId",
		Group:   "test",
		Content: "hello world!222222"})
	if err!=nil ||!success {
		fmt.Println("publish failed",err)
	}

	//content, err := configClient.GetConfig(vo.ConfigParam{
	//	DataId: "go-test1.yaml",
	//	Group:  "test",
	//})
	//if err != nil {
	//	fmt.Println(err)
	//}
	//fmt.Println("content:" + content)

}
