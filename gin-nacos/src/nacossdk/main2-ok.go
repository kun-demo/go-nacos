package main

import (
	"fmt"
	"github.com/nacos-group/nacos-sdk-go/clients"
	"github.com/nacos-group/nacos-sdk-go/common/constant"
	"github.com/nacos-group/nacos-sdk-go/vo"
	"time"
)

func main() {
	sc := []constant.ServerConfig{{
		Scheme: "go-nacos-sdk",
		IpAddr: "192.168.0.204",
		ContextPath: "/nacos",
		Port:   8848,
	}}

	cc := constant.ClientConfig{
		//NamespaceId:         "e6295280-8bc8-424e-b9c6-2eb61da8a189", // 如果需要支持多namespace，我们可以场景多个client,它们有不同的NamespaceId。当namespace是public时，此处填空字符串。
		NamespaceId:         "test",
		TimeoutMs:           5000,
		NotLoadCacheAtStart: true,
		//LogDir:              "log",
		//CacheDir:            "cache",
		CacheDir:         "D:\\temp\\cache", //缓存目录
		LogDir:         "D:\\temp\\log", //日志目录
		RotateTime:          "1h",
		MaxAge:              3,
		LogLevel:            "debug",
	}

	configClient, err := clients.CreateConfigClient(map[string]interface{}{
		"serverConfigs": sc,
		"clientConfig":  cc,
	})
	if err != nil {
		panic(err)
	}



	content, err := configClient.GetConfig(vo.ConfigParam{
		DataId: "go-test1.yaml",
		Group:  "test",
	})

	fmt.Println("err:",err)
	if err != nil {
		panic(err)
	}
	fmt.Println(content) //字符串 - yaml
	err = configClient.ListenConfig(vo.ConfigParam{
		DataId: "go-test1.yaml",
		Group:  "test",
		OnChange: func(namespace, group, dataId, data string) {
			fmt.Println("配置文件发生了变化...")
			fmt.Println("group:" + group + ", dataId:" + dataId + ", data:" + data)
		},
	})

	time.Sleep(300 * time.Second)
}


