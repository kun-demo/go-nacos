package main

import (
	//client "/nacosGo/client"
	"fmt"
	"gin-nacos/nacosGo"
)

func main() {
	option:=nacosGo.Options.WithAdress("192.168.0.204").WithPort(8848)
	nacosClient:=nacosGo.NacosClient.NewNacos(option)
	if ok:=nacosClient.Publish("test","test","hello word");ok{
		fmt.Println("Nacos 配置列表")
	}
	content:=nacosClient.Get("test","test")
	fmt.Println("Nacos 配置内容:",content)
	if err:=nacosClient.Listen("test","test", func(namespace, group, dataId, data string) {
		fmt.Println("监听修改修改配置:",data)
	});err!=nil{
		panic(err)
	}
	if ok:=nacosClient.Delete("test","test");ok{
		fmt.Println("删除配置成功")
	}
	if ok :=nacosClient.Register("127.0.0.1",9000);ok{
		fmt.Println("注册服务成功")
	}
}
