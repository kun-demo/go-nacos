package main

import (
	"fmt"
	"github.com/nacos-group/nacos-sdk-go/clients"
	//_ "github.com/demo/pkg/logger" // 引入包时自动执行了 logger 包的 init 函数完成了日志初始化配置
	//"github.com/nacos-group/nacos-sdk-go/clients/naming_client"
	"github.com/nacos-group/nacos-sdk-go/common/constant"
	"github.com/nacos-group/nacos-sdk-go/vo"
	"github.com/spf13/viper"
	"strings"
)

func main()  {
	NacosInit()
}

func NacosInit(){

	clientConfig := constant.ClientConfig{
		NamespaceId:         "go-nacos", // 如果需要支持多namespace，我们可以场景多个client,它们有不同的NamespaceId。当namespace是public时，此处填空字符串。
		TimeoutMs:           5000,
		NotLoadCacheAtStart: true,
		//LogDir:              "log",
		//CacheDir:            "cache",
		RotateTime:          "1h",
		MaxAge:              3,
		LogLevel:            "debug",
	}

	serverConfigs := []constant.ServerConfig{
		{
			IpAddr:      "192.168.0.204", //此处可以使用网址和ip
			ContextPath: "/nacos",
			Port:        8848,
			Scheme:      "http",
		},
	}


	fmt.Printf("服务注册！")
	// 将服务注册到nacos
	namingClient,_ := clients.NewNamingClient(
		vo.NacosClientParam{
			ClientConfig:  &clientConfig,
			ServerConfigs: serverConfigs,
		},
	)

	fmt.Printf("获取nacos存在服务的信息！")
	namingClient.RegisterInstance(vo.RegisterInstanceParam{
		Ip:          "192.168.0.204",
		Port:        8848,
		ServiceName: "im-nacos-go",
		Weight:      10,
		Enable:      true,
		Healthy:     true,
		Ephemeral:   true,
		Metadata:    map[string]string{"idc":"shanghai"},
		ClusterName: "DEFAULT", // 默认值DEFAULT
		GroupName:   "test",   // 默认值DEFAULT_GROUP
	})
	fmt.Printf("获取nacos存在服务的信息！")
	//获取nacos存在服务的信息
	instance, err := namingClient.SelectOneHealthyInstance(vo.SelectOneHealthInstanceParam{
		ServiceName: "ljd-gateway",
		GroupName:   "test",             // 默认值DEFAULT_GROUP
		Clusters:    []string{"DEFAULT"}, // 默认值DEFAULT
	})
	fmt.Println(instance)
	fmt.Println(err)
	configClient, err := clients.NewConfigClient(
		vo.NacosClientParam{
			ClientConfig:  &clientConfig,
			ServerConfigs: serverConfigs,
		},
	)
	fmt.Printf("获取配置！")
	//获取配置
	configClientcontent, err := configClient.GetConfig(vo.ConfigParam{
		DataId: "go-test1.yaml",
		Group:  "test"})
	fmt.Println(configClientcontent)
	fmt.Println(err)

	fmt.Printf("解析yml格式的数据完成初始化！")
	//解析yml格式的数据完成初始化，上面的配置也可改成监听器
	v := viper.New()
	v.SetConfigType("yaml")
	v.ReadConfig(strings.NewReader(configClientcontent))
	logoText :=  v.GetString("logo")
	fmt.Println("logoText=",logoText)
}


