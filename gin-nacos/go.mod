module gin-nacos

go 1.16

require (
	gitee.com/jawide/go-micro-nacos v0.0.2
	github.com/asim/go-micro/v3 v3.7.1
	github.com/gin-gonic/gin v1.7.7
	github.com/nacos-group/nacos-sdk-go v1.0.6
	github.com/satori/go.uuid v1.2.0
	github.com/spf13/viper v1.7.1
	golang.org/x/text v0.3.7 // indirect
	//gopkg.in/go-playground/validator.v9 v9.29.1 // indirect
	gorm.io/gorm v1.23.2
)
